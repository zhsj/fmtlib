Source: fmtlib
Section: devel
Priority: optional
Maintainer: Shengjing Zhu <zhsj@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 cmake,
 googletest,
Build-Depends-Indep:
 dh-sequence-mkdocs,
 doxygen,
 mkdocs,
 mkdocstrings,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://fmt.dev/
Vcs-Browser: https://salsa.debian.org/zhsj/fmtlib
Vcs-Git: https://salsa.debian.org/zhsj/fmtlib.git

Package: libfmt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfmt11 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libfmt-doc,
Breaks:
 libspdlog-dev (<< 1:1.9.2+ds-0.1),
Conflicts:
 libcppformat1-dev,
 libcppformat2-dev,
 libfmt3-dev,
Description: fast type-safe C++ formatting library -- development files
 This library provides fast, type-safe, small, C++11-aware replacement of
 (s)printf and related machinery. In some cases it's noticeably faster
 than boost::format, boost::lexical_cast and even sprintf itself.
 .
 This package contains the development files, include the static library.

Package: libfmt-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${mkdocs:Depends},
Conflicts:
 libcppformat1-doc,
 libcppformat2-doc,
 libfmt3-doc,
Description: fast type-safe C++ formatting library -- documentation
 This library provides fast, type-safe, small, C++11-aware replacement of
 (s)printf and related machinery. In some cases it's noticeably faster
 than boost::format, boost::lexical_cast and even sprintf itself.
 .
 This package contains the documentation files.

Package: libfmt11
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: fast type-safe C++ formatting library -- library
 This library provides fast, type-safe, small, C++11-aware replacement of
 (s)printf and related machinery. In some cases it's noticeably faster
 than boost::format, boost::lexical_cast and even sprintf itself.
 .
 This package contains the shared library.
